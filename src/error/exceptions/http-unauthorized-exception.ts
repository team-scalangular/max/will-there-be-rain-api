import HttpException from './http-exception';

class HttpUnauthorizedException extends HttpException {
  constructor() {
    super(401, "Unauthorized");
  }
}

export default HttpUnauthorizedException;
