import HttpException from './http-exception';

class DisplayNameTakenException extends HttpException {
  constructor(displayName: string) {
    super(400, `User with displayName: ${displayName} already exists`);
  }
}

export default DisplayNameTakenException;
