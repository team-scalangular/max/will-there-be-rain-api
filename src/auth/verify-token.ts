import {Request, Response} from 'express';
import * as admin from 'firebase-admin';
import HttpUnauthorizedException from '../error/exceptions/http-unauthorized-exception';

export default function verifyAuthToken(req: Request, res: Response, next: any): void {
  if (req.headers.authtoken) {
    admin.auth().verifyIdToken(req.headers.authtoken as string)
      .then(() => {
        next();
      })
      .catch(() => {
        next(new HttpUnauthorizedException());
      });
  } else if (req.method === 'GET') {
    next();
  } else {
    next(new HttpUnauthorizedException());
  }
}
