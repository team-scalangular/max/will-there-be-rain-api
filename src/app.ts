import express from 'express';
import * as admin from 'firebase-admin';
import {ServiceAccount} from 'firebase-admin/lib/credential';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import {mongooseConnectionConfig} from './utils/mongoose-connection.config';
import errorMiddleware from './error/error.middleware';
import Controller from './models/controller';
import ignoreFavicon from './utils/ignore-favicon.middleware';
import {corsConfig} from './utils/cors.config';
import verifyAuthToken from './auth/verify-token';

const morgan = require('morgan');
const cors = require('cors');
const serviceAccount = require('../config/serviceaccount.json');

class App {
  public app: express.Application;
  private basePath = '/api/v1';

  constructor(controllers: Controller[]) {
    this.app = express();
    this.app.disable('etag');

    App.initializeFirebaseAuth();
    App.connectToTheDatabase();
    this.initializeMiddlewares();
    this.initializeControllers(controllers);
    this.initializeErrorHandling();
  }

  private static initializeFirebaseAuth(): void {
    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount as ServiceAccount),
      databaseURL: process.env.FIREBASE_URL,
    });
  }

  private static connectToTheDatabase(): void {
    const {
      MONGO_USER,
      MONGO_PASSWORD,
      MONGO_PATH,
    } = process.env;
    mongoose.connect(`mongodb+srv://${MONGO_USER}:${MONGO_PASSWORD}${MONGO_PATH}`, mongooseConnectionConfig);
  }

  public listen(): void {
    this.app.listen(process.env.PORT, () => {
      console.log(`App listening on the port ${process.env.PORT}`);
    });
  }

  private initializeMiddlewares(): void {
    this.app.use(bodyParser.json());
    this.app.use(morgan('dev'));
    this.app.use(cors(corsConfig));
    this.app.use(ignoreFavicon);
    this.app.use('/', verifyAuthToken);
  }

  private initializeControllers(controllers: Controller[]): void {
    controllers.forEach((controller: Controller) => {
      this.app.use(this.basePath, controller.router);
    });
  }

  private initializeErrorHandling(): void {
    this.app.use(errorMiddleware);
  }
}

export default App;
