import {ConnectOptions} from 'mongoose';

export const mongooseConnectionConfig: ConnectOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
};
