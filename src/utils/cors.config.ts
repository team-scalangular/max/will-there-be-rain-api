import {CorsOptions} from 'cors';

const whitelist = ['http://localhost:4200', 'http://localhost:8080'];

export const corsConfig: CorsOptions = {
  origin(origin, callback) {
    // allow requests with no origin
    if (!origin) return callback(null, true);
    if (whitelist.indexOf(origin) === -1) {
      const message = `The CORS policy for this origin doesn't allow access from the particular origin.`;
      return callback(new Error(message), false);
    }
    return callback(null, true);
  },
  optionsSuccessStatus: 200
};
