import {NextFunction, Request, Response} from 'express';

function ignoreFavicon(req: Request, res: Response, next: NextFunction) {
  if (req.originalUrl && req.originalUrl.split('/').pop()?.includes('favicon')) {
    return res.sendStatus(204);
  }
  return next();
}

export default ignoreFavicon;
