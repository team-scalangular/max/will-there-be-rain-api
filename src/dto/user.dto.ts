import { IsString } from 'class-validator';

/*
  All variables required to add or update a User (Mongo Attributes (_id, __v) are not required)
 */
class CreateUserDto {
  @IsString()
  public firebaseUid: string;

  // eslint-disable-next-line react/static-property-placement
  @IsString()
  public displayName: string;

  @IsString({each: true})
  public tourRefs: string[];
}

export default CreateUserDto;
