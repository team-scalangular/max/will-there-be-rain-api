import * as mongoose from 'mongoose';

export interface User extends mongoose.Document {
  firebaseUid: string;
  displayName: string;
  tourRefs: string[];
  _id?: string;
  __v?: number;
}

export const userSchema = new mongoose.Schema({
  firebaseUid: {type: String, required: true},
  displayName: {type: String, required: true},
  tourRefs: {type: [String], required: true}
});

export const userModel = mongoose.model<User>('User', userSchema);
