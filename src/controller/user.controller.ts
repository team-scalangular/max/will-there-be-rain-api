import * as express from 'express';
import {NextFunction, Request, Response} from 'express';
import {User, userModel} from '../models/user.model';
import Controller from '../models/controller';
import UserNotFoundException from '../error/exceptions/user-not-found-exception';
import HttpException from '../error/exceptions/http-exception';
import validationMiddleware from '../dto/dto-validation.middleware';
import CreateUserDto from '../dto/user.dto';
import DisplayNameTakenException from '../error/exceptions/displayname-taken-exception';

class UserController implements Controller {
  public path = '/users';
  public router = express.Router();
  private userModel = userModel;

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes(): void {
    this.router
      .get(this.path, this.getAllUsers)
      .get(`${this.path}/:id`, this.getUserById)
      .patch(`${this.path}/:id`, validationMiddleware(CreateUserDto, true), this.modifyUser)
      .delete(`${this.path}/:id`, this.deleteUser)
      .post(this.path, validationMiddleware(CreateUserDto), this.createUser);
  }

  private getAllUsers = (req: Request, res: Response, next: NextFunction): void => {
    this.userModel.find()
      .then((users: User[]) => {
        res.send(users);
      }).catch((error) => next(new HttpException(error.status, error.message)));
  };

  private getUserById = (req: Request, res: Response, next: NextFunction): void => {
    const id = req.params.id;
    this.userModel.findOne({firebaseUid: id})
      .then((user: User | null) => {
        if (user) {
          res.send(user);
        } else {
          next(new UserNotFoundException(id));
        }
      }).catch((error) => next(new HttpException(error.status, error.message)));
  };

  private modifyUser = (req: Request, res: Response, next: NextFunction): void => {
    const id = req.params.id;
    const userToPatch: User = req.body;
    this.userModel.findByIdAndUpdate(id, userToPatch, {new: true})
      .then((patchedUser: User | null) => {
        if (patchedUser) {
          res.send(patchedUser);
        } else {
          next(new UserNotFoundException(id));
        }
      }).catch((error) => next(new HttpException(error.status, error.message)));
  };

  private deleteUser = (req: Request, res: Response, next: NextFunction): void => {
    const id = req.params.id;
    this.userModel.findByIdAndDelete(id)
      .then((deletedUser: User | null) => {
        if (deletedUser) {
          res.sendStatus(200);
        } else {
          next(new UserNotFoundException(id));
        }
      }).catch((error) => next(new HttpException(error.status, error.message)));
  };

  private createUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const userData: User = req.body;
    const userToCreate = new this.userModel(userData);
    if (await this.userModel.findOne({displayName: userToCreate.displayName})) {
      next(new DisplayNameTakenException(userToCreate.displayName));
    } else {
      userToCreate.save()
        .then((savedUser: User) => {
          res.send(savedUser);
        }).catch((error) => next(new HttpException(error.status, error.message)));
    }
  };
}

export default UserController;
