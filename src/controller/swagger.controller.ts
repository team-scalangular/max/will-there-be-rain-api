import express from 'express';
import Controller from '../models/controller';

const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');

class SwaggerController implements Controller {
  public path = '/docs';
  public router = express.Router();
  private swaggerDefinition = YAML.load('./openapi.yaml');

  constructor() {
    this.initializeSwaggerUi();
  }

  private initializeSwaggerUi(): void {
    this.router
      .use(this.path, swaggerUi.serve)
      .get(this.path, swaggerUi.setup(this.swaggerDefinition));
  }
}

export default SwaggerController;
