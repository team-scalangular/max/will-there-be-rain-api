import dotenv from 'dotenv';
import UserController from './src/controller/user.controller';
import App from './src/app';
import validateEnv from './src/utils/validate-env';
import SwaggerController from './src/controller/swagger.controller';

validateEnv();
dotenv.config();

const app = new App(
  [
    new UserController(),
    new SwaggerController()
  ]
);

app.listen();
